package com.example.demo.services;

import com.example.demo.entity.Account;
import com.example.demo.entity.Customer;
import com.example.demo.repository.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    //
    public List<Customer> getAllCustomers() {
        return this.customerRepository.findAll();
    }

    public void deleteCustomerById(Long id) {
        this.customerRepository.deleteById(id);
    }

    public Optional<Customer> getCustomerById(Long id) {
        return this.customerRepository.findById(id);
    }

    public void addCustomer(Customer customer) {
        this.customerRepository.save(customer);
    }

    public Customer editCustomerById(Customer customer) throws Exception {
        customer.setAccounts(this.customerRepository.findById(customer.getId()).orElseThrow(Exception::new).getAccounts());

        return this.customerRepository.save(customer);
    }

    public void addAccountToCustomerById(Long id, Account account) {
        this.customerRepository.findById(id).map(customer -> {
            customer.addAccount(account);

            return this.customerRepository.save(customer);
        });
    }

//    Dont know smart realization
//    public Optional<Customer> editCustomerById(Customer customer) {
//        String newName = customer.getName() == null ? customerDao.getOne(customer.getId()).getName() : customer.getName();
//        String newEmail = customer.getEmail() == null ? customerDao.getOne(customer.getId()).getEmail() : customer.getEmail();
//        int newAge = customer.getAge() == null ? customerDao.getOne(customer.getId()).getAge() : customer.getAge();
//        List<Account> newList = customer.getAccounts() == null ? customerDao.getOne(customer.getId()).getAccounts() : customer.getAccounts();
//
//        Customer result = new Customer(newName, newEmail, newAge, newList);
//
//        customerDao.edit(customer.getId() - 1, result);
//        return new ResponseEntity<>(HttpStatus.ACCEPTED);
//    }

//    ResponseEntity<Void> addAccountToCustomerById(@RequestBody Customer customer) {
//        Customer customerWithNewAccount = customerDao.getOne(customer.getId());
//        List<Account> oldAccounts = customerWithNewAccount.getAccounts();
//        List<Account> newAccounts = customer.getAccounts();
//
//        if (oldAccounts == null) {
//            customerWithNewAccount.setAccounts(newAccounts);
//        } else {
//            oldAccounts.addAll(newAccounts);
//
//            customerWithNewAccount.setAccounts(oldAccounts);
//        }
//
//
//        editCustomerById(customerWithNewAccount);
//        return new ResponseEntity<>(HttpStatus.ACCEPTED);
//    }
}
