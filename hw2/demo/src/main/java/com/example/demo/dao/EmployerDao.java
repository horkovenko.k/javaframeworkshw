package com.example.demo.dao;

import com.example.demo.entity.Employer;
import org.springframework.stereotype.Component;

@Component
public class EmployerDao extends AbstractDao<Employer> {
}
