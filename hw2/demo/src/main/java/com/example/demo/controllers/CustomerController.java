package com.example.demo.controllers;


import com.example.demo.dao.CustomerDao;
import com.example.demo.entity.Account;
import com.example.demo.entity.Customer;
import com.example.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get")
    ResponseEntity<List<Customer>> getAllCustomers() {
        return new ResponseEntity<>(customerService.getAllCustomers(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Customer> getCustomerById(@PathVariable Long id) {
        return new ResponseEntity<>(customerService.getCustomerById(id).get(), HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Void> addCustomer(@RequestBody Customer customer) {
        customerService.addCustomer(customer);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteCustomerById(@PathVariable Long id) {
        customerService.deleteCustomerById(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/{id}/account")
    ResponseEntity<Void> addAccountToCustomerById(@PathVariable Long id, @RequestBody Account account) {
        customerService.addAccountToCustomerById(id, account);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/edit")
    ResponseEntity<Void> editCustomerById(@RequestBody Customer customer) {
        try {
            customerService.editCustomerById(customer);

            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /*
    private final CustomerDao customerDao;

    @Autowired
    public CustomerController(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @GetMapping("/get")
    ResponseEntity<List<Customer>> getAllCustomers() {
        return new ResponseEntity<>(customerDao.findAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Customer> getCustomerById(@PathVariable Long id) {
        return new ResponseEntity<>(customerDao.getOne(id), HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Customer> getCustomerById(@RequestBody Customer customer) {
        return new ResponseEntity<>(customerDao.save(new Customer(customer.getName(), customer.getEmail(), customer.getAge(), customer.getAccounts())), HttpStatus.ACCEPTED);
    }

    @PutMapping("/edit")
    ResponseEntity<Void> editCustomerById(@RequestBody Customer customer) {
        String newName = customer.getName() == null ? customerDao.getOne(customer.getId()).getName() : customer.getName();
        String newEmail = customer.getEmail() == null ? customerDao.getOne(customer.getId()).getEmail() : customer.getEmail();
        int newAge = customer.getAge() == null ? customerDao.getOne(customer.getId()).getAge() : customer.getAge();
        List<Account> newList = customer.getAccounts() == null ? customerDao.getOne(customer.getId()).getAccounts() : customer.getAccounts();

        Customer result = new Customer(newName, newEmail, newAge, newList);

        customerDao.edit(customer.getId() - 1, result);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteCustomerById(@PathVariable Long id) {
        customerDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/account")
    ResponseEntity<Void> addAccountToCustomerById(@RequestBody Customer customer) {
        Customer customerWithNewAccount = customerDao.getOne(customer.getId());
        List<Account> oldAccounts = customerWithNewAccount.getAccounts();
        List<Account> newAccounts = customer.getAccounts();

        if (oldAccounts == null) {
            customerWithNewAccount.setAccounts(newAccounts);
        } else {
            oldAccounts.addAll(newAccounts);

            customerWithNewAccount.setAccounts(oldAccounts);
        }


        editCustomerById(customerWithNewAccount);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }*/
}
