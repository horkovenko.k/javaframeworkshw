package com.example.demo.services;

import com.example.demo.entity.Account;
import com.example.demo.entity.Customer;
import com.example.demo.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    //
    public List<Account> getAllAccounts() {
        return this.accountRepository.findAll();
    }

    public void deleteAccountById(Long id) {
        this.accountRepository.deleteById(id);
    }

    public void addAccount(Account account) {
        this.accountRepository.save(account);
    }

    public Optional<Account> putMoneyToAccountById(Long accountId, Double amount) {
        if (amount < 0) {
            return Optional.empty();
        }

        return accountRepository.findById(accountId).map(account -> {
            account.setBalance(account.getBalance() + amount);
            return accountRepository.save(account);
        });
    }


    public Optional<Account> withdrawMoneyFromAccountById(Long accountId, Double amount) {
        if (amount < 0) {
            return Optional.empty();
        }

        return accountRepository.findById(accountId)
                .filter(account -> account.getBalance() >= amount)
                .map(account -> {
                    account.setBalance(account.getBalance() - amount);
                    return accountRepository.save(account);
                });
    }

    public Optional<Account> transferMoneyBetweenAccounts(Long fromId, Long toId, Double amount) {
        if (amount < 0) {
            return Optional.empty();
        }

        Optional<Account> from = accountRepository.findById(fromId);
        Optional<Account> to = accountRepository.findById(toId);

        return from
                .filter(account -> account.getBalance() >= amount)
                .flatMap(f -> to.map(t -> {
                    f.setBalance(f.getBalance() - amount);
                    t.setBalance(t.getBalance() + amount);

                    accountRepository.saveAll(List.of(f, t));

                    return t;
                }));
    }
}
