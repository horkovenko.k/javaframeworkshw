package com.example.demo.controllers;

import com.example.demo.dao.CustomerDao;
import com.example.demo.dao.EmployerDao;
import com.example.demo.entity.Account;
import com.example.demo.entity.Customer;
import com.example.demo.entity.Employer;
import com.example.demo.services.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/employer")
public class EmployerController {
    private EmployerService employerService;

    public EmployerController(EmployerService employerService) {
        this.employerService = employerService;
    }

    @GetMapping("/get")
    ResponseEntity<List<Employer>> getAllEmployers() {
        return new ResponseEntity<>(employerService.getAllEmployers(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Employer> getEmployerById(@PathVariable Long id) {
        return new ResponseEntity<>(employerService.getEmployerById(id).get(), HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Void> addEmployer(@RequestBody Employer employer) {
        employerService.addNewEmployer(employer);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteCustomerById(@PathVariable Long id) {
        employerService.deleteEmployerById(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    /*
    private final EmployerDao employerDao;

    @Autowired
    public EmployerController(EmployerDao employerDao) {
        this.employerDao = employerDao;
    }


    @GetMapping("/get")
    ResponseEntity<List<Employer>> getAllCustomers() {
        return new ResponseEntity<>(employerDao.findAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/get/{id}")
    ResponseEntity<Employer> getCustomerById(@PathVariable Long id) {
        return new ResponseEntity<>(employerDao.getOne(id), HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Employer> getCustomerById(@RequestBody Employer employer) {
        return new ResponseEntity<>(employerDao.save(new Employer(employer.getName(), employer.getAddress())), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteCustomerById(@PathVariable Long id) {
        employerDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }*/
}
