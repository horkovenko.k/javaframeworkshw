package com.example.demo.dao;

import com.example.demo.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDao<T extends AbstractEntity> implements Dao<T> {
    protected List<T> list = new ArrayList<>();

    @Override
    public T save(T obj) {
        list.add(obj);

        return obj;
    }

    @Override
    public boolean delete(T obj) {
        if (list.contains(obj)) {
            list.remove(obj);

            return true;
        }

        return false;
    }

    @Override
    public void deleteAll(List<T> entities) {
        for (T c : entities) {
            list.removeIf(i -> c.getId().equals(i.getId()));
        }
    }

    @Override
    public void saveAll(List<T> entities) {
        list.addAll(entities);
    }

    @Override
    public List<T> findAll() {
        return list;
    }

    @Override
    public boolean deleteById(long id) {
        return list.removeIf(i -> i.getId() == id);
    }

    @Override
    public T getOne(long id) {
        return list.stream().filter(i -> i.getId() == id).findFirst().orElse(null);
    }

}
