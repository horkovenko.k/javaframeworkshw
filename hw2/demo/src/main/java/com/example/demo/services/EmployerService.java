package com.example.demo.services;

import com.example.demo.entity.Employer;
import com.example.demo.repository.EmployerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployerService {
    private EmployerRepository employerRepository;

    public EmployerService(EmployerRepository employerRepository) {
        this.employerRepository = employerRepository;
    }

    //
    public List<Employer> getAllEmployers() {
        return this.employerRepository.findAll();
    }

    public Optional<Employer> getEmployerById(Long id) {
        return this.employerRepository.findById(id);
    }

    public void addNewEmployer(Employer employer) {
        this.employerRepository.save(employer);
    }

    public void deleteEmployerById(Long id) {
        this.employerRepository.deleteById(id);
    }
}
