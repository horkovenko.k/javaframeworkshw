package com.example.demo.dao;

import com.example.demo.entity.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerDao extends AbstractDao<Customer> {
    public void edit(Long id, Customer obj) {
        list.set(Math.toIntExact(id), obj);
    }
}
