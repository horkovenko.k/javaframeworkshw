package com.example.demo.controllers;

import com.example.demo.dao.AccountDao;
import com.example.demo.dto.AccountDto;
import com.example.demo.entity.Account;
import com.example.demo.entity.Customer;
import com.example.demo.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/account")
public class AccountController {

    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/get")
    ResponseEntity<List<Account>> getAllAccounts() {
        return new ResponseEntity<>(this.accountService.getAllAccounts(), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteAccountById(@PathVariable Long id) {
        this.accountService.deleteAccountById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/put")
    ResponseEntity<Void> putMoneyToAccountById(@RequestBody Account account) {
        this.accountService.putMoneyToAccountById(account.getId(), account.getBalance());

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    ResponseEntity<Void> addAccount(@RequestBody Account account) {
        accountService.addAccount(account);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/withdraw")
    ResponseEntity<Void> withdrawMoneyFromAccountById(@RequestBody Account account) {
        this.accountService.withdrawMoneyFromAccountById(account.getId(), account.getBalance());

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/transfer")
    ResponseEntity<Void> transferMoneyBetweenAccounts(@RequestBody AccountDto accountDto) {
        this.accountService.transferMoneyBetweenAccounts(accountDto.getFrom(), accountDto.getTo(), accountDto.getBalance());


        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    /*
    private final AccountDao accountDao;

    @Autowired
    public AccountController(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @GetMapping("/get")
    ResponseEntity<List<Account>> getAllAccounts() {
        return new ResponseEntity<>(accountDao.findAll(), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity<Void> deleteAccountById(@PathVariable Long id) {
        accountDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/put")
    ResponseEntity<Void> putMoneyToAccountById(@RequestBody Account account) {
        accountDao.editOne(account.getId(), account);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/withdraw")
    ResponseEntity<Void> withdrawMoneyFromAccountById(@RequestBody Account account) {
        Account resultAccount = accountDao.getOne(account.getId());

        if (resultAccount != null && resultAccount.getBalance() >= account.getBalance()) {
            resultAccount.setBalance(resultAccount.getBalance() - account.getBalance());

            accountDao.editOne(account.getId(), resultAccount);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @PutMapping("/transfer")
    ResponseEntity<Void> transferMoneyBetweenAccounts(@RequestBody AccountDto accountDto) {
        Account fromAccount = accountDao.getOne(accountDto.getFrom());
        Account toAccount = accountDao.getOne(accountDto.getTo());

        if (fromAccount != null && toAccount != null && fromAccount.getBalance() >= accountDto.getBalance()) {
            fromAccount.setBalance(fromAccount.getBalance() - accountDto.getBalance());
            toAccount.setBalance(toAccount.getBalance() + accountDto.getBalance());

            accountDao.editOne(accountDto.getFrom(), fromAccount);
            accountDao.editOne(accountDto.getTo(), toAccount);

            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
*/
}
