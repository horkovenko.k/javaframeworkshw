package com.example.demo.dao;

import com.example.demo.entity.Account;
import com.example.demo.entity.Currency;
import com.example.demo.entity.Customer;
import org.springframework.stereotype.Component;

@Component
public class AccountDao extends AbstractDao<Account> {
    public void editOne(long id, Account account) {
        Account current = getOne(id);

        if (current != null) {
            String newNumber = account.getNumber() == null ? current.getNumber() : account.getNumber();
            Currency newCurrency = account.getCurrency() == null ? current.getCurrency() : account.getCurrency();
            Double newBalance = account.getBalance() == null ? current.getBalance() : account.getBalance();
            Customer newCustomer = account.getCustomer();

            Account result = new Account(newNumber, newCurrency, newBalance, newCustomer);
            list.set(Math.toIntExact(id) - 1, result);
        }
    }
}
