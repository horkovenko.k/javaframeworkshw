package com.example.demo.dao;


import com.example.demo.models.Account;
import com.example.demo.models.Currency;
import com.example.demo.models.Customer;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class AccountDao implements Dao<Account> {
    private List<Account> list = new ArrayList<>();
    {
        list.add(new Account(Currency.EUR, 1L));
        list.add(new Account(Currency.GBP, 2L));
        list.add(new Account(Currency.USD, 3L));
        list.add(new Account(Currency.USD, 3L));
        list.add(new Account(Currency.CHF, 4L));
        list.add(new Account(Currency.EUR, 5L));
        list.add(new Account(Currency.USD, 5L));
        list.add(new Account(Currency.GBP, 5L));
    }

    @Override
    public Account save(Account obj) {
        list.add(obj);

        return obj;
    }

    @Override
    public boolean delete(Account obj) {
        if (list.contains(obj)) {
            list.remove(obj);

            return true;
        }

        return false;
    }

    @Override
    public void deleteAll(List<Account> entities) {
        for (Account c : entities) {
            list.removeIf(i -> c.getId().equals(i.getId()));
        }
    }

    @Override
    public void saveAll(List<Account> entities) {
        list.addAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return list;
    }

    @Override
    public boolean deleteById(long id) {
        return list.removeIf(i -> i.getId() == id);
    }

    @Override
    public Account getOne(long id) {
        return list.stream().filter(i -> i.getId() == id).findFirst().orElse(null);
    }

    public void editOne(long id, Account account) {
        Account current = getOne(id);

        if (current != null) {
            Long newId = account.getId() == null ? current.getId() : account.getId();
            String newNumber = account.getNumber() == null ? current.getNumber() : account.getNumber();

            Currency newCurrency = account.getCurrency() == null ? current.getCurrency() : account.getCurrency();
            Double newBalance = account.getBalance() == null ? current.getBalance() : account.getBalance();

            Long newCustomer = account.getCustomer() == null ? current.getCustomer() : account.getCustomer();

            Account result = new Account(newId, newNumber, newCurrency, newBalance, newCustomer);
            list.set(Math.toIntExact(id) - 1, result);
        }
    }

}
