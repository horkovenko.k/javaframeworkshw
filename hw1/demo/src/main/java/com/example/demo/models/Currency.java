package com.example.demo.models;

public enum Currency {
    USD, EUR, UAH, CHF, GBP
}