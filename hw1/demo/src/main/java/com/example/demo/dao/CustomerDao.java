package com.example.demo.dao;

import com.example.demo.models.Customer;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class CustomerDao implements Dao<Customer> {
    private List<Customer> list = new ArrayList<>();

    {
        list.add(new Customer("Name1", "Email1", 100));
        list.add(new Customer("Name2", "Email2", 100));
        list.add(new Customer("Name3", "Email3", 100));
        list.add(new Customer("Name4", "Email4", 100));
        list.add(new Customer("Name5", "Email5", 100));
    }

    @Override
    public Customer save(Customer obj) {
        list.add(obj);

        return obj;
    }

    public void edit(Long id, Customer obj) {
        list.set(Math.toIntExact(id), obj);
    }

    @Override
    public boolean delete(Customer obj) {
        if (list.contains(obj)) {
            list.remove(obj);
            
            return true;
        }
        
        return false;
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        for (Customer c : entities) {
            list.removeIf(i -> c.getId().equals(i.getId()));
        }
    }

    @Override
    public void saveAll(List<Customer> entities) {
        list.addAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return list;
    }

    @Override
    public boolean deleteById(long id) {
        return list.removeIf(i -> i.getId() == id);
    }

    @Override
    public Customer getOne(long id) {
        return list.stream().filter(i -> i.getId() == id).findFirst().orElse(null);
    }
}
